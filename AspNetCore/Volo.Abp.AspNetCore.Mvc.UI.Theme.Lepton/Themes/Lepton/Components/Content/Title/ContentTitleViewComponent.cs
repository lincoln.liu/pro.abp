﻿using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc.UI.Layout;

namespace Volo.Abp.AspNetCore.Mvc.UI.Theme.Lepton.Themes.Lepton.Components.Content.Title
{
    public class ContentTitleViewComponent : LeptonViewComponentBase
	{
		private readonly IPageLayout _pageLayout;

		public ContentTitleViewComponent(IPageLayout pageLayout)
		{
			this._pageLayout = pageLayout;
		}

		public IViewComponentResult Invoke()
		{
			return base.View<string>("~/Themes/Lepton/Components/Content/Title/Default.cshtml", this._pageLayout.Content.Title);
		}
	}
}
