﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.Identity;
using Volo.Abp.Validation;

namespace Volo.Abp.Account.Public.Web.Pages.Account
{
	public class ForgotPasswordModel : AccountPageModel
	{
		[EmailAddress]
		[BindProperty]
		[DynamicStringLength(typeof(IdentityUserConsts), "MaxEmailLength", null)]
		[Required]
		public string Email { get; set; }

		[HiddenInput]
		[BindProperty(SupportsGet = true)]
		public string ReturnUrl { get; set; }

		[HiddenInput]
		[BindProperty(SupportsGet = true)]
		public string ReturnUrlHash { get; set; }

		public virtual Task<IActionResult> OnGetAsync()
		{
			return Task.FromResult<IActionResult>(this.Page());
		}

		public virtual async Task<IActionResult> OnPostAsync()
		{
			await base.AccountAppService.SendPasswordResetCodeAsync(new SendPasswordResetCodeDto
			{
				Email = this.Email,
				AppName = "MVC"
			});
			return this.RedirectToPage("./PasswordResetLinkSent");
		}
	}
}
