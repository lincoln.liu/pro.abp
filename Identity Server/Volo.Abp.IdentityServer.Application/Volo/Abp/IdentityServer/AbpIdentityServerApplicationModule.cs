﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace Volo.Abp.IdentityServer
{
    [DependsOn(
		typeof(AbpIdentityServerApplicationContractsModule),
		typeof(AbpAutoMapperModule),
		typeof(AbpIdentityServerDomainModule)
	)]
	public class AbpIdentityServerApplicationModule : AbpModule
	{
		public override void ConfigureServices(ServiceConfigurationContext context)
		{
			context.Services.AddAutoMapperObjectMapper<AbpIdentityServerApplicationModule>();

			Configure<AbpAutoMapperOptions>(options =>
			{
				options.AddProfile<AbpIdentityServerApplicationAutoMapperProfile>(true);
			});
		}
	}
}
