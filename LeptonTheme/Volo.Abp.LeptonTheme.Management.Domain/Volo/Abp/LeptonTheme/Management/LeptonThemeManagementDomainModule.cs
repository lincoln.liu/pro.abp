﻿using Volo.Abp.Modularity;

namespace Volo.Abp.LeptonTheme.Management
{
    [DependsOn(
		typeof(LeptonThemeManagementDomainSharedModule)
	)]
	public class LeptonThemeManagementDomainModule : AbpModule
	{
	}
}
