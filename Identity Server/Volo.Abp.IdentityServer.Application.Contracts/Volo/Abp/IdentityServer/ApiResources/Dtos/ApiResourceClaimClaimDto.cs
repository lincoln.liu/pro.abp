﻿using System;

namespace Volo.Abp.IdentityServer.ApiResources.Dtos
{
	public class ApiResourceClaimClaimDto
	{
		public Guid ApiResourceId { get; set; }

		public string Type { get; set; }
	}
}
