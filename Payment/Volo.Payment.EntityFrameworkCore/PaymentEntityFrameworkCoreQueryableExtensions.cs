﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using Volo.Payment.Requests;

namespace Volo.Payment.EntityFrameworkCore
{
    public static class PaymentEntityFrameworkCoreQueryableExtensions
	{
		public static IQueryable<PaymentRequest> IncludeDetails(this IQueryable<PaymentRequest> queryable, bool include = true)
		{
			if (!include)
			{
				return queryable;
			}
			return queryable.Include<PaymentRequest, ICollection<PaymentRequestProduct>>(x => x.Products);
		}
	}
}
