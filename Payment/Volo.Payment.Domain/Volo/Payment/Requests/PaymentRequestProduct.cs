﻿using System;
using System.Collections.Generic;
using Volo.Abp;
using Volo.Abp.Data;
using Volo.Abp.Domain.Entities;

namespace Volo.Payment.Requests
{
    public class PaymentRequestProduct : Entity, IHasExtraProperties
	{
		public Guid PaymentRequestId { get; private set; }

		public string Code { get; private set; }

		public string Name { get; private set; }

		public float UnitPrice { get; private set; }

		public int Count { get; private set; }

		public float TotalPrice { get; private set; }

		public Dictionary<string, object> ExtraProperties { get; protected set; }

		private PaymentRequestProduct()
		{
			this.ExtraProperties = new Dictionary<string, object>();
		}

		internal PaymentRequestProduct(Guid paymentRequestId, string code, string name, float unitPrice, int count = 1, float? totalPrice = null)
		{
			this.PaymentRequestId = paymentRequestId;
			this.Code = Check.NotNullOrWhiteSpace(code, nameof(code));
			this.Name = Check.NotNullOrWhiteSpace(name, nameof(name));
			this.UnitPrice = unitPrice;
			this.Count = count;
			this.TotalPrice = (totalPrice ?? (unitPrice * (float)count));
			this.ExtraProperties = new Dictionary<string, object>();
		}

		public override object[] GetKeys()
		{
			return new object[]
			{
				this.PaymentRequestId,
				this.Code
			};
		}
	}
}
