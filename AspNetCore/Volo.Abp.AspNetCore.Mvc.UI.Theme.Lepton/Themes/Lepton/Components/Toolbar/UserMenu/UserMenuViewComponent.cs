﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Volo.Abp.UI.Navigation;

namespace Volo.Abp.AspNetCore.Mvc.UI.Theme.Lepton.Themes.Lepton.Components.Toolbar.UserMenu
{
    public class UserMenuViewComponent : LeptonViewComponentBase
	{
		private readonly IMenuManager _menuManager;

		public UserMenuViewComponent(IMenuManager menuManager)
		{
			this._menuManager = menuManager;
		}

		public async Task<IViewComponentResult> InvokeAsync()
		{
			ApplicationMenu applicationMenu = await this._menuManager.GetAsync("User");
			ApplicationMenu model = applicationMenu;
			return base.View<ApplicationMenu>("~/Themes/Lepton/Components/Toolbar/UserMenu/Default.cshtml", model);
		}
	}
}
